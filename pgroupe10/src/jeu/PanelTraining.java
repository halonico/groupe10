package jeu;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
public class PanelTraining extends AbstractGame{
	private JButton jbMenu,jbPrevious,jbNext;
	/**
	 * This redefined method compare the typed answer with the question's answer.
	 * If the answer is correct, it goes to the next question
	 */
	protected void compare(String answer)
	{
		if(answer.length()>0)
		{
			if(Levenshtein.comparator(iterator.item().getAnswer(), answer, 4))
			{
				int tmp =Integer.parseInt(getJtfScore().getText())+1;
				getJtfScore().setText(Integer.toString(tmp));
				JOptionPane.showMessageDialog(null, "Good answer !");
				if(maxScore< Integer.parseInt(getJtfScore().getText()))
					maxScore= Integer.parseInt(getJtfScore().getText());
				nextQuestion();
			}
			else
			{
				JOptionPane.showMessageDialog(null, "Wrong answer !");
				//getJtfScore().setText("0");
			}
		}
		else
		{
			getJtfScore().setText("0");
			nextQuestion();
		}
	}
	/**
	 * Constructor of the training panel
	 */
	public PanelTraining() {
			this.setBackground(new Color(120, 82, 44));
			GroupLayout gl = new GroupLayout(this);
			this.setLayout(gl);
			gl.setAutoCreateContainerGaps(true);
			gl.setAutoCreateGaps(true);
			gl.linkSize(SwingConstants.VERTICAL, getJlAutor(), getJtfAutor(), getJlTheme(), getJtfTheme(),getJlScore(),getJtfScore());
			gl.linkSize(SwingConstants.VERTICAL, getJlAnswer(), getJtfAnswer(), getJbClear(), getJbOK());
			gl.setHorizontalGroup(gl.createParallelGroup()
					.addGroup(gl.createSequentialGroup()
							.addComponent(getJlTheme())
							.addComponent(getJtfTheme())
							.addComponent(getJlAutor())
							.addComponent(getJtfAutor())
							.addComponent(getJlScore())
							.addComponent(getJtfScore())
						)
					.addComponent(getJpCluesContainer())
					.addGroup(gl.createSequentialGroup()
							.addComponent(getJlAnswer())
							.addComponent(getJtfAnswer())
							.addComponent(getJbClear())
							.addComponent(getJbPrevious())
							.addComponent(getJbOK())
							.addComponent(getJbNext())
							.addComponent(getJbMenu())
						)
				);
		gl.setVerticalGroup(gl.createSequentialGroup()
				.addGroup(gl.createParallelGroup()
						.addComponent(getJlTheme())
						.addComponent(getJtfTheme())
						.addComponent(getJlAutor())
						.addComponent(getJtfAutor())
						.addComponent(getJlScore())
						.addComponent(getJtfScore())						)
				.addComponent(getJpCluesContainer())
				.addGap(50)
				.addGroup(gl.createParallelGroup()
						.addComponent(getJlAnswer())
						.addComponent(getJtfAnswer())
						.addComponent(getJbClear())
						.addComponent(getJbPrevious())
						.addComponent(getJbOK())
						.addComponent(getJbNext())
						.addComponent(getJbMenu())
					)
				);
	}
	/**
	 * This method return the previous JButton, if we click on it, it goes to the previous question
	 * @return The previous JButton
	 */
	public JButton getJbPrevious() {
		if(jbPrevious==null)
		{
			jbPrevious = Design.createButton("<");
			jbPrevious.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					previousQuestion();
					
				}
			});
		}
		return jbPrevious;
	}
	/**
	 * This method return the next JButton, if we click on it, it goes to the next question
	 * @return The next JButton
	 */
	public JButton getJbNext() {
		if(jbNext==null)
		{
			jbNext = Design.createButton(">");
			jbNext.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					nextQuestion();
					
				}
			});
		}
		return jbNext;
	}
	/**
	 * This method return the JTextField's answer, if the user press enter on it, the game will launch
	 * If the game is already started, it compares the typed answer with the question's answer
	 */
	public JTextField getJtfAnswer() {
		if(jtfAnswer==null){
			jtfAnswer= Design.createTextField("Press enter to launch the game");
			jtfAnswer.addKeyListener(new KeyListener() {
				
				@Override
				public void keyTyped(KeyEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void keyReleased(KeyEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void keyPressed(KeyEvent e) {
					if(e.getKeyCode()==10)
					{
						if(!start)
						{
							start = true;
							nextQuestion();
						}
						else
						{
							compare(getJtfAnswer().getText());
						}
						
					}
					
				}
			});
		}
		return jtfAnswer;
	}
	/**
	 * This method return the menu JButton, when we click on it, it shows the MenuPanel
	 * @return The menu JButton
	 */
	public JButton getJbMenu() {
		if(jbMenu==null){
			jbMenu= Design.createButton("Menu");
			jbMenu.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					// TODO Auto-generated method stub
					end();
				}
				
			});
		}
		return jbMenu;
	}
	/**
	 * This method goes the next question
	 */
	protected void nextQuestion()
	{
		iterator.next();
		setQuestion();
	}
	/**
	 * This method goes the previous question
	 */
	private void previousQuestion()
	{
		iterator.previous();
		setQuestion();
	}
	/**
	 * This method set the current question for the game and start a new theard to show every clue
	 */
	public void setQuestion()
	{
		clear();
		if(cluesTimer!=null)
		{
			cluesTimer.interrupt();
		}
		getJtfAnswer().setText("");
		if(iterator.item()!=null)
		{
			getJtfAutor().setText(iterator.item().getAuthor());
			List<String>clues = iterator.item().getClues();
			getJtfClue1().setText(clues.get(0));
			getJtfClue2().setText(clues.get(1));
			getJtfClue3().setText(clues.get(2));
			getJtfTheme().setText(iterator.item().getTheme());
			//show the first clue
			showClue(0);
			cluesTimer = new Thread(new Runnable() {
				public void run() {
					for(int i=1;i<=3;i++)
					{
						showClue(i);
						try {
							Thread.sleep(clueTime);
						} catch (InterruptedException e) {
							clear();
							i=4;
						}
					}
				}
			});
			cluesTimer.start();
		}
		else
		{
			end();
		}
		
	}
	/**
	 * This method prepare the game before the launch by the user.
	 */
	public void prepare()
	{
		start=false;
		iterator=Deck.getInstance().getIterator();
		iterator.setIndex(0);
		maxScore=0;
		getJtfAnswer().setText("Press enter to start ...");
		getJtfAutor().setText("");
		getJtfTheme().setText("");
		getJtfScore().setText("0");
	}
	/**
	 * This function is called when there is no more question.
	 */
	
	
	public void end()
	{
		try {
			cluesTimer.interrupt();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		JOptionPane.showMessageDialog(getParent(), "Your score is "+maxScore);
		GraphicalInterface gi =(GraphicalInterface) getJbOK().getTopLevelAncestor();
		gi.changePanel("Menu");
	}
	@Override
	  protected void paintComponent(Graphics g) {

	    super.paintComponent(g);
	    URL path= getClass().getResource("/img/GameBackground.jpg");
		Image tmp = new ImageIcon(path).getImage();
	        g.drawImage(tmp, 0, 0, getWidth(),getHeight(),this);
	}
}

