package jeu;

import java.util.List;
public class IteratorQuestion {
	private int index;
	private List<Question> listeQuestions;
	public IteratorQuestion(List<Question> liste) 
	{	
		super();
		this.listeQuestions = liste;
	}
	/**
	 * Increment the index
	 */
	public void next() 
	{	
		index++;	
	}
	/**
	 * Decrement the index
	 */
	public void previous()
	{
		index--;
	}
	/**
	 * Set the index at a specific position
	 * @param index The index position
	 */
	public void setIndex(int index)
	{
		if(index>=0)
		{
			this.index = index;
		}
	}
	/**
	 * Reach a question with a specified theme
	 * @param theme Choosen theme
	 */
	public void reachItem(String theme)
	{
		for(;;)
		{
			Question q =item();
			if(q==null)
			{
				return;
			}
			if(item().getTheme().equals(theme))
			{
				return;
			}
			next();
		}
	}
	/**
	 * Access the item
	 * @return The selected Question
	 */
	public Question item() 
	{
	
		if (index < listeQuestions.size())		 
		{	return listeQuestions.get(index);}	
		else 
		{	return null;			}	
										
	}
	
}
