package jeu;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
class ModelTableauScore extends AbstractTableModel{
	
	
	private String[] nomcol={"Player","Serie","Timer","Score"};
	@Override
	public String getColumnName(int column) {
		// TODO Auto-generated method stub
		return nomcol[column];
	}
	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return nomcol.length;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return GestionScore.getScores().size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
		Score s= GestionScore.getScores().get(rowIndex) ;
		String result="";
		switch(columnIndex){
		case 0:
			result=s.getLogin() ;
			break;
		case 1:result=String.valueOf(s.getSerie());
			break;
		case 2:result=String.valueOf(s.getTimer());
			break;
		case 3: result = String.valueOf(s.getScore());
		}
		return result;
	}
	
	
}
public class PanelScore extends JPanel{
	private JTable jtScore;
	private JScrollPane jscrolltable;
	private JButton jbMenu;
	public PanelScore() {
		GroupLayout gl = new GroupLayout(this);
		this.setLayout(gl);
		gl.setAutoCreateContainerGaps(true);
		gl.setAutoCreateGaps(true);
		//gl.linkSize(SwingConstants.HORIZONTAL, getjscrolltable(),getJbMenu());
		gl.setHorizontalGroup(gl.createParallelGroup()
				.addGap(0,100,Integer.MAX_VALUE)
				.addComponent(getjscrolltable())
				.addGroup(gl.createSequentialGroup()
						.addGap(0,100,Integer.MAX_VALUE)
						.addComponent(getJbMenu())
						.addGap(0,100,Integer.MAX_VALUE)
						)
				.addGap(0,100,Integer.MAX_VALUE)
				);
		gl.setVerticalGroup(gl.createSequentialGroup()
				.addGap(0,100,Integer.MAX_VALUE)
				.addComponent(getjscrolltable())
				.addComponent(getJbMenu())
				.addGap(0,100,Integer.MAX_VALUE)
				);
	}
	@Override
	  protected void paintComponent(Graphics g) {

	    super.paintComponent(g);
	    URL path= getClass().getResource("/img/GameBackground.jpg");
		Image tmp = new ImageIcon(path).getImage();
	        g.drawImage(tmp, 0, 0, getWidth(),getHeight(),this);
	}
	public JTable getJtScore() {
		if(jtScore==null){
			jtScore= Design.createJTable(15f);
			jtScore.setModel(new ModelTableauScore());
		}
		return jtScore;
	}
	public JScrollPane getjscrolltable() {
		if(jscrolltable==null){
			jscrolltable = new JScrollPane(getJtScore());
			jscrolltable.getViewport().setBackground(new Color(134, 105, 76));
			getJtScore().setOpaque(false);
		}
		return jscrolltable;
	}
	public JButton getJbMenu()
	{
		if(jbMenu==null)
		{
			jbMenu = Design.createButton("Menu");
			jbMenu.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					GraphicalInterface tmp =(GraphicalInterface) getjscrolltable().getTopLevelAncestor();
					tmp.changePanel("Menu");
				}
			});
		}
		return jbMenu;
	}
}
