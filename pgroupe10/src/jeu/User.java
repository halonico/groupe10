package jeu;

import java.util.ArrayList;

public class User {
	private String userName;
	private String password;
	private ArrayList<Integer> scores;
	public User(String userName,String password)
	{
		this.userName = userName;
		this.password = password;
		scores = new ArrayList<Integer>();
	}
	public boolean addScore(int score)
	{
		if(score>0)
		{
			scores.add(score);
			return true;
		}
		return false;
	}
}
