package jeu;

import javax.swing.*;
import java.awt.*;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.*;
public class PanelTheme extends JPanel {
	private ArrayList<JButton> bTheme = new ArrayList<JButton>();
	private ArrayList<String> themes;
	private String themeMyst;
	private JLabel jlTitle;
	public PanelTheme(){
		this.setBackground(new Color(120, 82, 44));
		Deck deck = Deck.getInstance();
		themes =deck.getThemes();
		for(int i=0; i<=3; i++){
			if(i<3){
				bTheme.add(createButton("..."));
			}else{
				bTheme.add(createButton("?"));
			}
		}
		JPanel jp = new JPanel();
		jp.setSize(1000, 1000);
		jp.setBackground(null);
		jp.setOpaque(false);
		GridBagLayout gl = new GridBagLayout();
		this.setLayout(gl);
		this.add(jp);
		GridLayout gl2 = new GridLayout(1,4,20,0);
		jp.setLayout(gl2);
		for(int i=0;i<4;i++)//
		{
			bTheme.get(i).setMinimumSize(new Dimension(130,260));
			bTheme.get(i).setPreferredSize(new Dimension(200,400));
			bTheme.get(i).setMaximumSize(new Dimension(350,700));
			jp.add(bTheme.get(i));
		}
		/*gl.setHorizontalGroup(gl.createSequentialGroup()
				.addComponent(jp)
				//.addGap(Integer.MAX_VALUE,Integer.MAX_VALUE,Integer.MAX_VALUE)
				.addComponent(bTheme.get(0))
				.addComponent(bTheme.get(1))
				.addComponent(bTheme.get(2))
				.addComponent(bTheme.get(3))
				//.addGap(Integer.MAX_VALUE,Integer.MAX_VALUE,Integer.MAX_VALUE)
				);
		gl.setVerticalGroup(gl.createParallelGroup()
				.addComponent(jp)
				//.addGap(Integer.MAX_VALUE,Integer.MAX_VALUE,Integer.MAX_VALUE)
				.addComponent(bTheme.get(0))
				.addComponent(bTheme.get(1))
				.addComponent(bTheme.get(2))
				.addComponent(bTheme.get(3))
				//.addGap(Integer.MAX_VALUE,Integer.MAX_VALUE,Integer.MAX_VALUE)
				);*/
	}
	/**
	 * This method create a nice button with a theme on it 
	 * @param pickTheme The theme
	 * @return The formatted button
	 */
	private JButton createButton(String pickTheme) {
		JButton tmp = Design.createButton(pickTheme);
		tmp.setRolloverEnabled(true);
		URL path= getClass().getResource("/img/CardBackGround.jpg");
		tmp.setIcon(new ImageIcon(path));
		URL path2= getClass().getResource("/img/CardBackGroundHover.jpg");
		tmp.setRolloverIcon(new ImageIcon(path2));
		tmp.setVerticalTextPosition(SwingConstants.CENTER); 
		tmp.setHorizontalTextPosition(SwingConstants.CENTER);
		//GROS PBL DE FDP <3
		// String = htmlString.replaceAll("\\<.*?>","");
		tmp.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JButton o = (JButton)e.getSource();
				String name;
				//System.out.println(o.getText().substring(36,o.getText().length()-11));
				if(o.getText()!="?"){
					 name= o.getText().substring(36,o.getText().length()-11);
				}else{
					name=themeMyst;
				}
				GraphicalInterface tmp =(GraphicalInterface) bTheme.get(0).getTopLevelAncestor();
				tmp.getJpGame().setChosenTheme(name);
				tmp.changePanel("Game");
				tmp.getJpGame().prepare();
				tmp.getJpGame().getJtfTheme().setText(name);
			}
		});
		return tmp;
	}
	/**
	 * This method pick a theme form the loaded list one.
	 * @return The picked theme
	 */
	public String pickTheme(){
		int x =(int) (Math.random()*themes.size());
		String tmp = themes.get(x);
		if(themes.size()>=1)
		{
			themes.remove(x);
		}
		return tmp;
	}
	public JLabel getJlTitle() {
		if(jlTitle==null)
		{
			jlTitle= Design.createLabel("Choose your theme");
		}
		return jlTitle;
	}
	public void prepare()
	{
		for(int i=0; i<=3; i++){
			if(i<3){
				bTheme.get(i).setText("<html><p style='text-align: center'>"+pickTheme()+"</p></html>");
			}else{
				themeMyst=pickTheme();
			}
		}
	}
	@Override
	  protected void paintComponent(Graphics g) {

	    super.paintComponent(g);
	    URL path= getClass().getResource("/img/GameBackground.jpg");
		Image tmp = new ImageIcon(path).getImage();
	        g.drawImage(tmp, 0, 0, getWidth(),getHeight(),this);
	}
}

