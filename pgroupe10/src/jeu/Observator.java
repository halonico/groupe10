package jeu;


public interface Observator {
	public void actualise();
}
