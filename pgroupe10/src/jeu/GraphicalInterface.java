package jeu;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Frame;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import javax.swing.plaf.ColorUIResource;
class GraphicalInterface extends JFrame{
	private Point compCoords=null;
	private JPanel jpContainer,jpHead, jpMain;
	private PanelGame jpGame;
	private PanelMenu jpMenu;
	private PanelTheme jpTheme;
	private PanelTraining jpTraining;
	private PanelOption jpOption;
	private PanelScore jpScore;
	private PanelEndGame jpPanelEndGame;
	private JLabel jlTitle, img;
	private JButton jbClose,jbSize,jbMini;
	private boolean bool;
	
	Dimension tailleEcran=new Dimension(0,0);
	 Dimension miniDim=new Dimension(960,720);
	 
	public JLabel getImg() {
		if(img==null){
			img = new JLabel();
			URL path= getClass().getResource("/img/crown.png");
			Image image = new ImageIcon(path).getImage();
			image=image.getScaledInstance(20, 20, 1);
			img.setIcon(new ImageIcon(image));
			img.setSize(20,20);
		}
		return img;
	}


	public JPanel getJpMain() {
		if(jpMain==null){
			jpMain = new JPanel();
			GroupLayout gl = new GroupLayout(jpMain);
			jpMain.setLayout(gl);
			gl.setHorizontalGroup(gl.createParallelGroup()
					.addComponent(getJpHead())
					.addComponent(getJpContainer())
					);
			gl.setVerticalGroup(gl.createSequentialGroup()
					.addComponent(getJpHead())
					.addComponent(getJpContainer())
					);
			jpMain.addMouseListener(new MouseListener() {
				
				@Override
				public void mouseReleased(MouseEvent e) {
					// TODO Auto-generated method stub
					compCoords = null;
				}
				
				@Override
				public void mousePressed(MouseEvent e) {
					// TODO Auto-generated method stub
					 compCoords = e.getPoint();
				}
				
				@Override
				public void mouseExited(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mouseEntered(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mouseClicked(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
			});
			jpMain.addMouseMotionListener(new MouseMotionListener() {
				public void mouseMoved(MouseEvent e) {
	            }

	            public void mouseDragged(MouseEvent e) {
	                Point currCoords = e.getLocationOnScreen();     
	                GraphicalInterface gi =(GraphicalInterface)getJbClose().getTopLevelAncestor();
	                gi.setLocation(currCoords.x - compCoords.x, currCoords.y - compCoords.y);
	            }
	        });
		}
		return jpMain;
	}
	

	public JButton getJbSize() {
		if(jbSize==null)
		{
			jbSize=Design.createButtonHead("\u25A1");
			jbSize.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					GraphicalInterface gi =(GraphicalInterface)getJbClose().getTopLevelAncestor();
					if(bool){
						gi.setSize(miniDim);
					}
					else{
						gi.setExtendedState(Frame.MAXIMIZED_BOTH);
					}
					bool=!bool;
				}
			});
			
		}
		return jbSize;
	}

	public JButton getJbMini() {
		if(jbMini==null){
			jbMini=Design.createButtonHead("_");
			jbMini.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					GraphicalInterface gi =(GraphicalInterface)getJbClose().getTopLevelAncestor();
					gi.setState(Frame.ICONIFIED);
					
				}
			});
		}
		return jbMini;
	}

	public JLabel getJlTitle() {
		if(jlTitle==null){
			jlTitle = new JLabel("King of Questions");
			Font font = jlTitle.getFont().deriveFont(20f);
			jlTitle.setFont(font);
			jlTitle.setForeground(Design.getBgbutton());
			
		}
		return jlTitle;
	}
	public JButton getJbClose() {
		if(jbClose==null){
			jbClose =Design.createButtonHead("X");
			jbClose.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					// TODO Auto-generated method stub
					System.exit(0);
				}
			});
		}
		return jbClose;
	}
	public void quit(){
		int choice=JOptionPane.showConfirmDialog(null, "Are you sure ?");
		System.out.println(choice);
		if(choice==0){
			System.exit(0);
		}
	}
	public GraphicalInterface()
	{
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch(Exception err){}
		bool = false;
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		tailleEcran =Toolkit.getDefaultToolkit().getScreenSize();
		//this.setSize(640,480);
		this.setMinimumSize(miniDim);
		this.setUndecorated(true);
		//this.add(getJpHead());
		
		    this.getRootPane().setBorder(BorderFactory.createLineBorder(new Color(255,231,102),5));
		    this.setLocationRelativeTo(null);
		this.add(getJpMain());
	}
	public JPanel getJpContainer() {
		if(jpContainer ==null)
		{
			jpContainer = new JPanel();
			CardLayout cl = new CardLayout();
			jpContainer.setLayout(cl);
			jpContainer.add(getJpMenu(),"Menu");
			jpContainer.add(getJpTheme(),"Theme");
			jpContainer.add(getJpGame(),"Game");
			jpContainer.add(getJpScore(),"Score");
			jpContainer.add(getJpTraining(),"Training");
			jpContainer.add(getJpOption(),"Option");
			jpContainer.add(getJpPanelEndGame(),"EndGame");
		}
			
		return jpContainer;
	}
	
	public JPanel getJpHead() {
		if(jpHead==null){
			jpHead = new JPanel();
			jpHead.setBackground(Design.getBgpanel());
			GroupLayout gl = new GroupLayout(jpHead);
			jpHead.setLayout(gl);
			gl.setAutoCreateContainerGaps(true);
			gl.setHorizontalGroup(gl.createSequentialGroup()
					.addComponent(getImg())
					.addGap(10)
					.addComponent(getJlTitle())
					.addGap(0,100,Integer.MAX_VALUE)
					.addComponent(getJbMini())
					.addComponent(getJbSize())
					.addComponent(getJbClose())
					);
			gl.setVerticalGroup(gl.createParallelGroup()
					.addComponent(getImg())
					.addComponent(getJlTitle())
					.addComponent(getJbMini())
					.addComponent(getJbSize())
					.addComponent(getJbClose())
					);
		}
		return jpHead;
	}
	public PanelEndGame getJpPanelEndGame() {
		if(jpPanelEndGame==null){
			jpPanelEndGame=new PanelEndGame();
		}
		return jpPanelEndGame;
	}
	public PanelGame getJpGame() {
		if(jpGame == null)
		{
			jpGame = new PanelGame();
		}
		return jpGame;
	}
	public PanelOption getJpOption() {
		if(jpOption == null)
		{
			jpOption = new PanelOption();
		}
		return jpOption;
	}
	public PanelMenu getJpMenu() {
		if(jpMenu ==null)
		{
			jpMenu = new PanelMenu();
		}
		return jpMenu;
	}
	public PanelTheme getJpTheme() {
		if(jpTheme == null)
		{
			jpTheme = new PanelTheme();
		}
		return jpTheme;
	}
	public PanelScore getJpScore()
	{
		if(jpScore==null)
		{
			jpScore= new PanelScore();
		}
		return jpScore;
	}
	public PanelTraining getJpTraining()
	{
		if(jpTraining==null)
		{
			jpTraining = new PanelTraining();
		}
		return jpTraining;
	}
	public void changePanel(String panelName)
	{
		CardLayout cl = (CardLayout) getJpContainer().getLayout();
		cl.show(getJpContainer(), panelName);
	}
	public void addPanel(JPanel panel)
	{
		this.add(panel);
	}
//

}
