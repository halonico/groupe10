package jeu;

import java.lang.reflect.Type;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class GestionScore {
	private static ArrayList<Score> scores;
	//Static non sérialisable
	public static void loadScores()
	{
		Gson gson = new Gson();
		String tmp = Persistance.lireJson("Scores");
		Type listType = new TypeToken<ArrayList<Score>>(){}.getType();
		scores =  gson.fromJson(tmp, listType);
	}
	public static void saveScores()
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(scores);
		Persistance.creerJson("scores", json);
		System.out.println("je suis appelé");
	}
	public static void addScore(Score s)
	{
		if(scores==null)
		{
			scores = new ArrayList<Score>();
		}
		scores.add(s);
	}
	public static ArrayList<Score> getScores() {
		if(scores==null)
		{
			scores=new ArrayList<Score>();
		}
		return scores;
	}
	
}

