package jeu;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class SoundPlayer {
	private static Clip mainSound;
	private static Clip effect;
	public static void playMainSound(final String url) {
		      try {
		        mainSound = AudioSystem.getClip();
		        //Get the clip instance of the AudioSystem
		        AudioInputStream inputStream = AudioSystem.getAudioInputStream(
		       Deck.class.getResource("/sounds/" + url));
		       //Get the sound file inside the sounds folder
		        mainSound.open(inputStream);
		        mainSound.loop(Clip.LOOP_CONTINUOUSLY);
		        mainSound.start(); 
		        
		        //play
		      } catch (Exception e) {
		        System.err.println(e.getMessage());
		      }
		    }
	public static void playEffectSound(final String url) {
	      try {
	       effect = AudioSystem.getClip();
	        //Get the clip instance of the AudioSystem
	        AudioInputStream inputStream = AudioSystem.getAudioInputStream(
	       Deck.class.getResource("/sounds/" + url));
	       //Get the sound file inside the sounds folder
	        effect.open(inputStream);
	       effect.start();
	        //play
	      } catch (Exception e) {
	        System.err.println(e.getMessage());
	      }
	    }
	public static void stopEffectSound()
	{
		effect.stop();
	}
	public static void stopMainSound()
	{
		mainSound.stop();
	}
}
