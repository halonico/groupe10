package jeu;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ContainerListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
//
public class PanelMenu extends JPanel{
	private JButton jbSolo,jbOnline,jbTraining,jbScore,jbExit,jbOptions;
    private Font font;
	public JButton getJbSolo() {
		if(jbSolo ==null)
		{
			jbSolo = Design.createButton("Solo");
			jbSolo.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					GraphicalInterface tmp =(GraphicalInterface) getJbExit().getTopLevelAncestor();
					tmp.changePanel("Theme");
					tmp.getJpTheme().prepare();
				}
			});
		}
		return jbSolo;
	}

	public JButton getJbOnline() {
		if( jbOnline==null)
		{
			jbOnline = Design.createButton("Online");	
		}
		return jbOnline;
	}

	public JButton getJbTraining() {
		if(jbTraining ==null)
		{
			jbTraining= Design.createButton("Training");
			jbTraining.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					GraphicalInterface tmp =(GraphicalInterface) getJbExit().getTopLevelAncestor();
					tmp.changePanel("Training");
					tmp.getJpTraining().prepare();
				}
			});
		}
		return jbTraining;
	}

	public JButton getJbScore() {
		if( jbScore==null)
		{
			jbScore= Design.createButton("Scores");
			jbScore.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					GraphicalInterface tmp =(GraphicalInterface) getJbExit().getTopLevelAncestor();
					((ModelTableauScore) tmp.getJpScore().getJtScore().getModel()).fireTableDataChanged();
					tmp.changePanel("Score");
				}
			});
		}
		return jbScore;
	}
	public JButton getJbExit() {
		if( jbExit==null)
		{
			jbExit= Design.createButton("Exit");
			jbExit.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					int choice=JOptionPane.showConfirmDialog(getParent(), "Are you sure ?");
					System.out.println(choice);
					if(choice==0){
						System.exit(0);
					}
				}
			});
			
		}
		return jbExit;
	}

	public JButton getJbOptions() {
		if(jbOptions ==null)
		{
			jbOptions = Design.createButton("Options");
			jbOptions.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					GraphicalInterface tmp =(GraphicalInterface) getJbExit().getTopLevelAncestor();
					tmp.changePanel("Option");
					SoundPlayer.stopMainSound();
				}
			});
		}
		return jbOptions;
	}


	@Override
	  protected void paintComponent(Graphics g) {

	    super.paintComponent(g);
	    URL path= getClass().getResource("/img/MenuBackground.jpg");
		Image tmp = new ImageIcon(path).getImage();
	        g.drawImage(tmp, 0, 0, getWidth(),getHeight(),this);
	}
	public PanelMenu()
	{
		GroupLayout gl = new GroupLayout(this);
		this.setLayout(gl);
		gl.setAutoCreateContainerGaps(true);
		gl.setAutoCreateGaps(true);
		//gl.linkSize(SwingConstants.HORIZONTAL, getJbSolo(),getJbOnline(),getJbTraining());
		gl.setHorizontalGroup(gl.createParallelGroup()
				
			.addGroup(gl.createParallelGroup(GroupLayout.Alignment.CENTER)
				.addGap(100, 800, Integer.MAX_VALUE)
				.addComponent(getJbSolo())
				.addComponent(getJbOnline())
				.addComponent(getJbTraining())
				.addComponent(getJbScore())
				.addGroup(gl.createSequentialGroup()
					.addComponent(getJbExit())
					.addComponent(getJbOptions())
					)
			)
			
		);
		gl.setVerticalGroup(gl.createSequentialGroup()
				
				.addGap(100, 500, Integer.MAX_VALUE-100)
				.addComponent(getJbSolo())
				.addComponent(getJbOnline())
				.addComponent(getJbTraining())
				.addComponent(getJbScore())
				.addGroup(gl.createParallelGroup()
						.addComponent(getJbExit())
						.addComponent(getJbOptions())						
						)
				.addGap(0, 600, Integer.MAX_VALUE)
				);
		Component [] compo = this.getComponents();
		for(Component c: compo){
			c.setPreferredSize(new Dimension(200,30));
			c.setMaximumSize(new Dimension(200,30));
		}
		SoundPlayer.playMainSound("MenuSound.wav");
	}


}
