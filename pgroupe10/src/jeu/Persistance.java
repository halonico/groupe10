package jeu;
//
import java.io.*;
import java.util.Scanner;
public class Persistance {
	public static void creerJson(String nom, String questions){
		try (OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(nom+".json"))){
			out.write(questions);
			}
			catch(IOException e) {
			System.err.println(e.getMessage());
			}
	}
	public static String lireJson(String fichier) {
		String lectMessage="";
		try(Scanner scan = new Scanner(new FileReader(fichier+".json"))) {
			do{
				if(scan.hasNextLine())
				{
					lectMessage+=scan.nextLine();
				}
			}while(scan.hasNextLine());
			return lectMessage;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
