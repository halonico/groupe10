package jeu;


import java.util.ArrayList;
import java.util.List;

public abstract class Observer {
	private List<Observator> observators = new ArrayList<>();;
	
	public boolean addObservator(Observator o){
		if(!observators.contains(o)){
			return observators.add(o);
		}
		return false;
	}
	public boolean deleterObservateur(Observator o){
		return observators.remove(o);
	}
	public void notifie(){
		for(Observator obs : observators){
			obs.actualise();
		}
	}
}
