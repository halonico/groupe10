package jeu;

import java.util.ArrayList;
import java.util.List;
//
public class Question {
	private String author;
	private String answer;
	private String theme;
	private List<String> clues;
	
	public Question(String author, String answer, String theme, List<String> clues) {
		this.author = author;
		this.answer = answer;
		this.theme = theme;
		this.clues = clues;
		
	}
	public Question(String author, String answer, String theme, String clue1,String clue2,String clue3) {
		this.author = author;
		this.answer = answer;
		this.theme = theme;
		clues = new ArrayList<String>();
		clues.add(clue1);
		clues.add(clue2);
		clues.add(clue3);
		
	}
	public String toString()
	{
		String tmp = "";
		tmp+="Author : "+author+"\n";
		tmp+="Answer : "+answer+"\n";
		tmp+="Theme : "+theme+"\nClues : ";
		for(String clue : clues)
		{
			tmp+=clue+"\n";
		}
		
		return tmp;
	}
	public List<String> getClues() {
		return clues;
	}
	public String getAnswer() {
		return answer;
	}
	public boolean equals(Object o)
	{
		if(o instanceof Question)
		{
			Question tmp = (Question)o;
			return (this.theme.equals(tmp.theme) && this.answer.equals(tmp.answer));
		}
		return false;
	}
	public String getAuthor() {
		return author;
	}
	public String getTheme() {
		return theme;
	}
	public Question copy()
	{
		return new Question(this.author, this.answer, this.theme, this.clues);
	}
}