package jeu;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.LookAndFeel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
public class PanelGame extends AbstractGame{
	private JLabel jlTimer;
	private JButton jbClear, jbOK;
	private JProgressBar jpbTimer;
	private Thread gameTimer;
	private static String chosenTheme;
	public int time = 40;
	public static int maxTime=40;
	/**
	 * This redefined method compare the typed answer with the question's answer.
	 * After that, it goes to the next question
	 */
	
	
	protected void compare(String answer)
	{
		if(answer.length()>0)
		{
			/**
			 * If the typed answer and the question's answer have 4 differences or less,
			 * the typed answer is correct
			 */
			if(Levenshtein.comparator(iterator.item().getAnswer(), answer, 4))
			{
				int tmp =Integer.parseInt(getJtfScore().getText())+1;
				getJtfScore().setText(Integer.toString(tmp));
				SoundPlayer.playEffectSound("GoodSound.wav");
				JOptionPane.showMessageDialog(getParent(), "Good answer !");
				if(maxScore< Integer.parseInt(getJtfScore().getText()))
					maxScore= Integer.parseInt(getJtfScore().getText());
			}
			else
			{
				JOptionPane.showMessageDialog(getParent(), "Wrong answer !");
				getJtfScore().setText("0");
			}
		}
		else
		{
			getJtfScore().setText("0");
		}
		nextQuestion();

	}
	/**
	 * Constructor of the Panel
	 */
	public PanelGame() {
			this.setBackground(new Color(120, 82, 44));
			GroupLayout gl = new GroupLayout(this);
			this.setLayout(gl);
			gl.setAutoCreateContainerGaps(true);
			gl.setAutoCreateGaps(true);
			gl.linkSize(SwingConstants.VERTICAL, getJlAutor(), getJtfAutor(), getJlTheme(), getJtfTheme(),getJlScore(),getJtfScore(),getJlTimer(),getJpbTimer());
			gl.linkSize(SwingConstants.VERTICAL, getJlAnswer(), getJtfAnswer(), getJbClear(), getJbOK());
			gl.setHorizontalGroup(gl.createParallelGroup()
					.addGroup(gl.createSequentialGroup()
							.addComponent(getJlTheme())
							.addComponent(getJtfTheme())
							.addComponent(getJlAutor())
							.addComponent(getJtfAutor())
							.addComponent(getJlScore())
							.addComponent(getJtfScore())
							.addComponent(getJlTimer())
							.addComponent(getJpbTimer())
						)
					.addComponent(getJpCluesContainer())
					.addGroup(gl.createSequentialGroup()
							.addComponent(getJlAnswer())
							.addComponent(getJtfAnswer())
							.addComponent(getJbClear())
							.addComponent(getJbOK())
						)
				);
		gl.setVerticalGroup(gl.createSequentialGroup()
				.addGroup(gl.createParallelGroup()
						.addComponent(getJlTheme())
						.addComponent(getJtfTheme())
						.addComponent(getJlAutor())
						.addComponent(getJtfAutor())
						.addComponent(getJlScore())
						.addComponent(getJtfScore())
						.addComponent(getJlTimer())
						.addComponent(getJlTimer())
						.addComponent(getJpbTimer())						)
				.addComponent(getJpCluesContainer())
				.addGap(50)
				.addGroup(gl.createParallelGroup()
						.addComponent(getJlAnswer())
						.addComponent(getJtfAnswer())
						.addComponent(getJbClear())
						.addComponent(getJbOK())							
					)
				);
	}
	/**
	 * This method return the JTextField's answer, if the user press enter on it, the game will launch
	 * If the game is already started, it compares the typed answer with the question's answer
	 */
	public JTextField getJtfAnswer() {
		if(jtfAnswer==null){
			jtfAnswer= Design.createTextField("Press enter to launch the game");
			jtfAnswer.addKeyListener(new KeyListener() {
				
				@Override
				public void keyTyped(KeyEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void keyReleased(KeyEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void keyPressed(KeyEvent e) {
					if(e.getKeyCode()==10)
					{
						if(!start)
						{
							start = true;
							iterator.setIndex(-1);
							gameTimer.start();
							SoundPlayer.stopMainSound();
							nextQuestion();
						}
						else
						{
							compare(getJtfAnswer().getText());
						}
						
					}
					
				}
			});
		}
		return jtfAnswer;
	}
	/**
	 * Getter for the first clue
	 * @return
	 */
	public JLabel getJlTimer() {
		if(jlTimer==null)
		{
			jlTimer = Design.createLabel("Time");
		}
		return jlTimer;
	}
	/**
	 * Return the timer under a JProgressBar form
	 * @return The timer 
	 */
	public JProgressBar getJpbTimer()
	{
		if(jpbTimer==null)
		{
		
			    LookAndFeel previousLF = UIManager.getLookAndFeel();
			    try {
			        UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
			        jpbTimer = new JProgressBar(0,maxTime);
					jpbTimer.setForeground(new Color(0,240,0));
					jpbTimer.setString(String.valueOf(maxTime));
			        UIManager.setLookAndFeel(previousLF);
			    } catch (IllegalAccessException | UnsupportedLookAndFeelException | InstantiationException | ClassNotFoundException e) {}
		}
		return jpbTimer;
	}
	/**
	 * This method set the chosen theme for the game
	 * @param themeName The chosen theme
	 */
	public void setChosenTheme(String themeName)
	{
		chosenTheme=themeName;
		getJtfTheme().setText(chosenTheme);
	}
	/**
	 * This method goes the next question with the corresponding theme
	 */
	public void nextQuestion()
	{
		iterator.next();
		iterator.reachItem(chosenTheme);
		setQuestion();
	}
	/**
	 * This method set the current question for the game and start a new theard to show every clue
	 */
	public void setQuestion()
	{
		clear();
		if(cluesTimer!=null)
		{
			cluesTimer.interrupt();
		}
		getJtfAnswer().setText("");
		if(iterator.item()!=null)
		{
			getJtfAutor().setText(iterator.item().getAuthor());
			List<String>clues = iterator.item().getClues();
			getJtfClue1().setText(clues.get(0));
			getJtfClue2().setText(clues.get(1));
			getJtfClue3().setText(clues.get(2));
			getJtfTheme().setText(iterator.item().getTheme());
			//show the first clue
			showClue(0);
			cluesTimer = new Thread(new Runnable() {
				public void run() {
					for(int i=1;i<=3;i++)
					{
						showClue(i);
						try {
							Thread.sleep(clueTime);
						} catch (InterruptedException e) {
							clear();
							i=4;
						}
					}
				}
			});
			cluesTimer.start();
		}
		else
		{
			end();
		}
		
	}
	/**
	 * This method prepare the game before the launch by the user.
	 * It's called from PanelTheme
	 */
	public void prepare()
	{
		clear();
		start=false;
		maxScore=0;
		time=maxTime;
		iterator = Deck.getInstance().getIterator();
		iterator.setIndex(0);
		getJtfAnswer().setText("Press enter to start ...");
		getJtfAutor().setText("");
		getJtfTheme().setText("");
		getJtfScore().setText("0");
		getJpbTimer().setForeground(new Color(0,240,0));
		getJpbTimer().setString(String.valueOf(time));
		getJpbTimer().setMaximum(maxTime);
		getJpbTimer().setValue(maxTime);
		gameTimer = new Thread(new Runnable() {
			public void run() {
				while(time>0)
				{
					
					time--;
					getJpbTimer().setString(""+time);
					getJpbTimer().setValue(time);
					getJpbTimer().setForeground(getColor(((double)time/(double)maxTime)));
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {}
				}
				end();
			}
		});	
	}
	/**
	 * This function create a color from green to red based on the percentage of time left.
	 * @param power The percentage of time (0 to 1)
	 * @return The corresponding color
	 */
	public Color getColor(double power)
	{
	    double H = power * 0.4; // Hue (note 0.4 = Green, see huge chart below)
	    double S = 0.9; // Saturation
	    double B = 0.9; // Brightness

	    return Color.getHSBColor((float)H, (float)S, (float)B);
	}
	/**
	 * This function is called when there is no more question or no time left.
	 */
	public void end()
	{
		time=0;
		cluesTimer.interrupt();
		GraphicalInterface gi =(GraphicalInterface) getJbOK().getTopLevelAncestor();
		gi.getJpPanelEndGame().setValues((int)(maxScore*(100./(double)maxTime)),maxScore,maxTime);
		SoundPlayer.stopMainSound();
		SoundPlayer.playMainSound("EndSound.wav");
		gi.changePanel("EndGame");
	}
	@Override
	  protected void paintComponent(Graphics g) {

	    super.paintComponent(g);
	    URL path= getClass().getResource("/img/GameBackground.jpg");
		Image tmp = new ImageIcon(path).getImage();
	        g.drawImage(tmp, 0, 0, getWidth(),getHeight(),this);
	}
}