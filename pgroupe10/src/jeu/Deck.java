package jeu;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Timer;
import java.util.concurrent.TimeUnit;
import java.lang.reflect.Type;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class Deck extends Observer {
	private ArrayList<Question> questions;
	private ArrayList<String> themes;
	private static Deck instance;
	private IteratorQuestion iterator;
	public void loadQuestions(){
		Gson gson = new Gson();
		String tmp = Persistance.lireJson("Questions");
		Type listType = new TypeToken<ArrayList<Question>>(){}.getType();
		questions =  gson.fromJson(tmp, listType);
		for(Question q:questions){
			if(!themes.contains(q.getTheme())){
				themes.add(q.getTheme());
			}
		}
	}
	private Deck()
	{
		questions = new ArrayList<Question>();
		themes = new ArrayList<String>();
		loadQuestions();
		iterator = new IteratorQuestion(questions);
		
	}
	public static Deck getInstance(){
		if(instance == null)
		{
			instance = new Deck();
		}
		return instance ;
	}
	public boolean addQuestion(String author, String answer, String theme, String clue1,String clue2,String clue3)
	{
		Question tmp = new Question(author, answer, theme, clue1,clue2,clue3);
		return addQuestion(tmp);
	} 
	public boolean addQuestion(Question q){
		if(!questions.contains(q))
		{
			questions.add(q);
			notifie();
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////:
			if(!themes.contains(q.getTheme())){
				themes.add(q.getTheme());
			}
			return true;
		}
		return false;
	}
	public void deleteQuestion(int i) {
		if(i>=0)
		{
			questions.remove(i);
		}
		notifie();
	}
	public boolean setQuestion(int i,Question q){
		questions.set(i, q);
		notifie();//
		return true;
	}
	public ArrayList<Question> getAllQuestions()
	{
		ArrayList<Question> tmp = new ArrayList<Question>();
		for(Question q : questions)
		{
			tmp.add(q.copy());
		}
		return tmp;
	}
	public IteratorQuestion getIterator()
	{
		return iterator;
	}
	public ArrayList<String> getThemes() {
		return (ArrayList<String>) themes.clone();
	}
	public void saveQuestions()
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(questions);
		Persistance.creerJson("Questions", json);
	}
	public Question get(int i) {
		return questions.get(i) ;
	}
	
}
