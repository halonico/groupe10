package jeu;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LookAndFeel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableCellRenderer;

public class Design {
	private static Color bgbutton= new Color(134, 105, 76);
	private static Color bgpanel= new Color(200, 180, 130);
	private static Color borderbutton=new Color(255,231,102);
	private static Color fontcolor= new Color(255,231,102);
	private static Font font;
	public static Color getBgbutton() {
		return bgbutton;
	}
	public static void setBgbutton(Color bgbutton) {
		Design.bgbutton = bgbutton;
	}
	public static Color getBgpanel() {
		return bgpanel;
	}
	public static void setBgpanel(Color bgpanel) {
		Design.bgpanel = bgpanel;
	}
	public static Color getBorderbutton() {
		return borderbutton;
	}
	public static void setBorderbutton(Color borderbutton) {																		
		Design.borderbutton = borderbutton;
	}
	public static Color getFontcolor() {
		return fontcolor;
	}
	public static void setFontcolor(Color fontcolor) {
		Design.fontcolor = fontcolor;
	}
	public static void loadRessources()
	{
		InputStream stream = Design.class.getResourceAsStream("/font/Belwen.otf");
		try {
			font = Font.createFont(Font.TRUETYPE_FONT, stream);
		} catch (FontFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * This method create every label with the style applied on it
	 * @param text The label's text
	 * @return The styled label
	 */
	public static JLabel createLabel(String text)
	{
		return createLabel(text,20f);
	}
	public static JLabel createLabel(String text,float fontSize)
	{
		JLabel label = new JLabel(text);
		label.setFont(font.deriveFont(fontSize));
		label.setForeground(fontcolor);
		return label;
	}
	public static JButton createButton(String text)
	{
		return createButton(text,20f);
	}
	public static JButton createButton(String text,float fontSize)
	{
		JButton button = new JButton();
		button.setFont(font.deriveFont(fontSize));
		button.setForeground(fontcolor);
		button.setBackground(bgbutton);
		Border line = new LineBorder(borderbutton, 5);
		Border margin = new EmptyBorder(5, 15, 5, 15);
		Border compound = new CompoundBorder(line, margin);
		button.setBorder(compound);
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				SoundPlayer.playEffectSound("ButtonSound.wav");
				
			}
		});
		//
		//writing html allow a long text to be multi-line
		button.setText(/*"<html><p style='text-align:center;'>"+*/text/*+"</p></html>"*/);
		return button;
	}
	public static JButton createButtonHead(String text)
	{
		JButton button = new JButton(text);
		button.setForeground(fontcolor);
		button.setBackground(bgbutton);
		return button;
	}
	public static JPanel createPanel()
	{
		JPanel panel = new JPanel();
		//panel.setForeground(new Color(255,231,102));
		panel.setBackground(bgpanel);
		panel.setBorder(new EmptyBorder(10, 10, 10, 10));
		return panel;
	}
	public static JTextField createTextField(String text)
	{
		return createTextField(text,20f);
	}
	public static JTextField createTextField(String text,float fontSize)
	{
		JTextField textField = new JTextField(text);
		textField.setMinimumSize(new Dimension(125, 15));
		textField.setFont(font.deriveFont(fontSize));
		textField.setForeground(fontcolor);
		textField.setBackground(new Color(134, 105, 76));
		Border line = new LineBorder(new Color(	255,231,102), 5);
		Border margin = new EmptyBorder(5, 15, 5, 15);
		Border compound = new CompoundBorder(line, margin);
		textField.setBorder(compound);
		return textField;
	}
	public static JTable createJTable(float fontSize)
	{JTable jtScore = null;
		 LookAndFeel previousLF = UIManager.getLookAndFeel();
		    try {
		        UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
		        jtScore = new JTable();
				jtScore=new JTable(new ModelTableauScore());
				jtScore.getTableHeader().setReorderingAllowed(false);
				jtScore.setFillsViewportHeight(true);
				jtScore.setBackground(new Color(134, 105, 76));
				jtScore.setForeground(new Color(255,231,102));
				jtScore.setFont(font.deriveFont(fontSize));
				jtScore.setForeground(fontcolor);
				jtScore.getTableHeader().setBackground(bgpanel);
				jtScore.getTableHeader().setForeground(bgbutton);
				jtScore.getTableHeader().setFont(font.deriveFont(30f));
				jtScore.setShowGrid(false);
				jtScore.setIntercellSpacing(new Dimension(0, 0));
				DefaultTableCellRenderer stringRenderer = (DefaultTableCellRenderer)
					     jtScore.getDefaultRenderer(String.class);
					stringRenderer.setHorizontalAlignment(SwingConstants.CENTER);
		        UIManager.setLookAndFeel(previousLF);
		    } catch (IllegalAccessException | UnsupportedLookAndFeelException | InstantiationException | ClassNotFoundException e) {}
		return jtScore;
	}
}
