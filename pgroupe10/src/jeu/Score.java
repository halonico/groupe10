
package jeu;

public class Score {
	private String login;
	private int score;
	private int serie;
	private int timer;
	public Score(String login,int score,int serie,int timer)
	{
		this.login = login;
		this.score = score;
		this.serie = serie;
		this.timer = timer;
	}
	public int getScore()
	{
		return score;
	}
	public int getSerie()
	{
		return serie;
	}
	public String getLogin() {
		return login;
	}
	public int getTimer(){
		return timer;
	}
}
