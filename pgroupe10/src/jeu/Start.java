package jeu;

import java.awt.Component;
import java.util.List;
import java.util.Scanner;

import javax.swing.JFrame;

import com.google.gson.Gson;
//
public class Start {
	//private static Question q;
	public static void main(String[] args) {
		Deck.getInstance().loadQuestions();
		GestionScore.loadScores();
		Design.loadRessources();
		GraphicalInterface window = new GraphicalInterface();
		window.setVisible(true);
	}
}
