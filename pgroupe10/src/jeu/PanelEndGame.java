package jeu;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;

public class PanelEndGame extends JPanel{
	private JLabel jlTitle,jlBestSuite, jlAsk;
	private JTextField jtfScore,jtfLogin,jtfBestSuite;
	private JButton jbContinue;
	private int maxTime, finalScore,finalSuite;
	public PanelEndGame(){
		GroupLayout gl = new GroupLayout(this);
		this.setLayout(gl);
		gl.setAutoCreateContainerGaps(true);
		gl.setAutoCreateGaps(true);
		gl.setHorizontalGroup(gl.createParallelGroup(GroupLayout.Alignment.CENTER)
				.addGap(0, 800, Integer.MAX_VALUE)
				.addComponent(getJlTitle())
				.addComponent(getJtfScore())
				.addComponent(getJlBestSuit())
				.addComponent(getJtfBestSuit())
				.addComponent(getJlAsk())
				.addComponent(getJtfLogin())
				.addComponent(getJbContinue())
				);
		gl.setVerticalGroup(gl.createSequentialGroup()
				.addGap(0,800,Integer.MAX_VALUE)
				.addComponent(getJlTitle())
				.addComponent(getJtfScore())
				.addComponent(getJlBestSuit())
				.addComponent(getJtfBestSuit())
				.addComponent(getJlAsk())
				.addComponent(getJtfLogin())
				.addComponent(getJbContinue())
				.addGap(0,800,Integer.MAX_VALUE)
				);
	}
	public JLabel getJlTitle() {
		if(jlTitle==null)
			jlTitle=Design.createLabel("Your score is :");
		return jlTitle;
	}
	public JLabel getJlAsk() {
		if(jlAsk==null)
			jlAsk=Design.createLabel("Do you want to save it ?");
		return jlAsk;
	}
	public JTextField getJtfScore() {
		if(jtfScore==null){
			jtfScore=Design.createTextField(null,30f);
			jtfScore.setMinimumSize(new Dimension(150, 150));
			jtfScore.setPreferredSize(new Dimension(150, 150));
			jtfScore.setMaximumSize(new Dimension(150, 150));
			jtfScore.setFocusable(false);
			jtfScore.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return jtfScore;
	}
	public JTextField getJtfLogin() {
		if(jtfLogin==null){
			jtfLogin=Design.createTextField("");
			jtfLogin.setMinimumSize(new Dimension(0, 60));
			jtfLogin.setPreferredSize(new Dimension(200, 30));
			jtfLogin.setMaximumSize(new Dimension(200, 30));
			jtfLogin.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return jtfLogin;
	}
	public JButton getJbContinue() {
		if(jbContinue==null)
			jbContinue=Design.createButton("Continue");
			jbContinue.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					GraphicalInterface gi =(GraphicalInterface) getJtfLogin().getTopLevelAncestor();
					if(getJtfLogin().getText().length()>1)
					{
						GestionScore.addScore(new Score(getJtfLogin().getText(),finalScore,finalSuite,maxTime));
						GestionScore.saveScores();
						getJtfLogin().setText("");
					}
					SoundPlayer.stopMainSound();
					SoundPlayer.playMainSound("MenuSound.wav");
					gi.changePanel("Menu");
				}
			});
		return jbContinue;
	}
	@Override
	  protected void paintComponent(Graphics g) {

	    super.paintComponent(g);
	    URL path= getClass().getResource("/img/GameBackground.jpg");
		Image tmp = new ImageIcon(path).getImage();
	        g.drawImage(tmp, 0, 0, getWidth(),getHeight(),this);
	}
	public JLabel getJlBestSuit() {
		if(jlBestSuite == null)
		{
			jlBestSuite= Design.createLabel("Your best suite : ", 20f);
		}
		return jlBestSuite;
	}
	public JTextField getJtfBestSuit() {
		if(jtfBestSuite==null)
		{
			jtfBestSuite = Design.createTextField("0", 15f);
			jtfBestSuite.setMinimumSize(new Dimension(75, 75));
			jtfBestSuite.setPreferredSize(new Dimension(75, 75));
			jtfBestSuite.setMaximumSize(new Dimension(75, 75));
			jtfBestSuite.setFocusable(false);
			jtfBestSuite.setHorizontalAlignment(SwingConstants.CENTER);
			
		}
		return jtfBestSuite;
	}
	public void setValues(int score,int suite,int maxTime)
	{
		finalScore=score;
		finalSuite=suite;
		this.maxTime=maxTime;
		getJtfScore().setText(String.valueOf(score));
		getJtfBestSuit().setText(String.valueOf(suite));
		
	}
	
}
