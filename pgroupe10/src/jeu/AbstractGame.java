package jeu;

import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.PaintContext;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
/**
 * This class is a template for every class that inherit him
 * All they have in common is there, some methods needs to be redefined
 * @author halonico
 *
 */
public abstract class AbstractGame extends JPanel{
	protected IteratorQuestion iterator;
	protected int maxScore=0;
	private JPanel jpCluesContainer;
	private JLabel jlTheme, jlAnswer, jlAuthor, jlScore;
	private JTextField jtfTheme, jtfAutor, jtfClue1, jtfClue2, jtfClue3, jtfScore;
	protected JTextField jtfAnswer;
	private JButton jbClear, jbOK;
	protected Thread cluesTimer;
	protected boolean start = false;
	public static long clueTime=4000;
	/**
	 * Show every clue one by one
	 * @param index The choosen clue
	 */
	public void showClue(int index)
	{
		switch(index)
		{
			case 1 : 
				jtfClue1.setVisible(true);
			break;
			case 2 : 
				jtfClue2.setVisible(true);
			break;
			case 3 : 
				jtfClue3.setVisible(true);
			break;
		}
	}
	/**
	 * Hide every clue
	 */
	public void clear()
	{
		jtfClue1.setVisible(false);
		jtfClue2.setVisible(false);
		jtfClue3.setVisible(false);
	}
	public JLabel getJlTheme() {
		if(jlTheme ==null)
		{
			jlTheme= Design.createLabel("Theme : ");
		}
		return jlTheme;
	}
	/**
	 * Show the current theme
	 * @return The JTextField theme
	 */
	public JTextField getJtfTheme() {
		if(jtfTheme==null)
		{
			jtfTheme = Design.createTextField("",13f);
			jtfTheme.setFocusable(false);
			jtfTheme.setBorder(javax.swing.BorderFactory.createEmptyBorder());
		}
		return jtfTheme;
	}
	public JLabel getJlAutor() {
		if(jlAuthor==null)
		{
			jlAuthor = Design.createLabel("Author : ");
		}
		return jlAuthor;
	
	}
	
	/**
	 * Show the question author
	 * @return The JTextField of the author
	 */
	public JTextField getJtfAutor() {
		if(jtfAutor==null){
			jtfAutor= Design.createTextField(null);
			jtfAutor.setEditable(false);
			jtfAutor.setBorder(javax.swing.BorderFactory.createEmptyBorder());
		}
		return jtfAutor;
	}
	public JLabel getJlAnswer() {
		if(jlAnswer==null){
			jlAnswer= Design.createLabel("Answer : ");
		}
		return jlAnswer;
	}
	/**
	 * Reset the JTextField answer to ""
	 * @return The clean JButton
	 */
	public JButton getJbClear() {
		if(jbClear==null){
			jbClear= Design.createButton("Clear");
			jbClear.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					// TODO Auto-generated method stub
					getJtfAnswer().setText("");
				}
				
			});
		}
		return jbClear;
	}
	/**
	 * Validate the answer only if the game is already started
	 * @return The ok JButton
	 */
	public JButton getJbOK() {
		if(jbOK==null){
			jbOK= Design.createButton("Ok");
			jbOK.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					if(start)
					{
						compare(getJtfAnswer().getText());
					}	
				}
				
		});
		}
		return jbOK;
	}
	public JLabel getJlScore() {
		if(jlScore == null)
		{
			jlScore = Design.createLabel("Score");
		}
		return jlScore;
	}
	/**
	 * Show the current score
	 * @return The score JTextField
	 */
	public JTextField getJtfScore() {
		if(jtfScore == null)
		{
			jtfScore = Design.createTextField("0");
			jtfScore.setBorder(javax.swing.BorderFactory.createEmptyBorder());
			jtfScore.setFocusable(false);
			jtfScore.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return jtfScore;
	}
	/**
	 * Show the first clue
	 * @return The first clue JTextField
	 */
	public JTextField getJtfClue1() {
		if(jtfClue1==null)
		{
			jtfClue1 = Design.createTextField(null);
			jtfClue1.setEditable(false);
		}
		return jtfClue1;
	}
	/**
	 * Show the second clue
	 * @return The second clue JTextField
	 */
	public JTextField getJtfClue2() {
		if(jtfClue2==null)
		{
			jtfClue2 = Design.createTextField(null);
			jtfClue2.setEditable(false);
		}
		return jtfClue2;
	}
	/**
	 * Show the third clue
	 * @return The second clue JTextField
	 */
	public JTextField getJtfClue3() {
		if(jtfClue3==null)
		{
			jtfClue3 =Design.createTextField(null);
			jtfClue3.setEditable(false);
		}
		return jtfClue3;
	}
	/**
	 * This panel contain every clues
	 * @return The clues container
	 */
	public JPanel getJpCluesContainer() {
		if(jpCluesContainer==null)
		{
			GridLayout layout = new GridLayout(3,1);
			jpCluesContainer=Design.createPanel();
			jpCluesContainer.setLayout(layout);
			layout.setVgap(20);
			jpCluesContainer.add(getJtfClue1());
			jpCluesContainer.add(getJtfClue2());
			jpCluesContainer.add(getJtfClue3());
		}
		return jpCluesContainer;
	}
	/**
	 * Every method needs to be redefined
	 */
	
	/**
	 * This method is called when there is no more question or when the timer is over
	 */
	protected abstract void end();
	/**
	 * This method is called before the game start
	 */
	protected abstract void prepare();
	/**
	 * This method is called to set the current question of the game
	 */
	protected abstract void setQuestion();
	/**
	 * This method is called to go to the next question
	 */
	protected abstract void nextQuestion();
	/**
	 * This return the answer JTextField, it needs to be redefined because of it's action listener
	 * @return The answer JTextField
	 */
	protected abstract JTextField getJtfAnswer();
	/**
	 * This method compare the answer with the question's answer
	 * @param answer The typed answer
	 */
	protected abstract void compare(String answer);
	
}