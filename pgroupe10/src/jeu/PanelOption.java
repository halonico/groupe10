package jeu;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JPopupMenu;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.colorchooser.ColorChooserComponentFactory;
import javax.swing.plaf.ColorChooserUI;
import javax.swing.table.AbstractTableModel;

import org.omg.CORBA.INTERNAL;

class ModelTableau extends AbstractTableModel{
	
	
	private String[] nomcol={"Author","Answer","Theme","Clues"};
	@Override
	public String getColumnName(int column) {
		// TODO Auto-generated method stub
		return nomcol[column];
	}
	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return nomcol.length;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return Deck.getInstance().getAllQuestions().size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
		Question q= Deck.getInstance().get(rowIndex) ;
		String result="";
		switch(columnIndex){
		case 0:
			result=q.getAuthor() ;
			break;
		case 1:result=q.getAnswer();
			break;
		case 2:result=q.getTheme();
			break;
		case 3: result = q.getClues().toString() ;
		}
		return result;
	}
	
}

public class PanelOption extends JPanel implements Observator{
	//private JPanel jpcountenair;
	private Component[] components;
	private JPanel jpoptionuser,jpoptionadmin,jpcolor,jplogin,jpform,jpQuestion,jpadmin,jpTab,jpTimeManagement;
	private JScrollPane jscrolluser,jscrolladmin,jscrolltable;
	private JTable jtable; 
	private final String login="admin";
	private final String pwd="helha";
	private JLabel jlbackground,jljpolicecolor,jlbuttoncolor,jlbordercolor,jltimer
					,jlauthor,jltheme,jlclue1,jlclue2,jlclue3,jlanswer,jlerror,jladmin,jlpwd,jlClueTime;
	private JTextField jttimer,jtauthor,jttheme,jtclue1,jtclue2,jtclue3,jtanswer,jtadmin;
	private JColorChooser jcbackground,jcfontcolor,jcbordercolor,jcbuttoncolor;
	private JButton jblogin,jblogout,jbmenu,jbbackground,jbadd,jbdel,jbModify,jbApply,jbSave,jbCancel;
	private JPasswordField jppwd;
	private Font font = new Font("Courier New", Font.ITALIC, 22);
	int row=-1;
	public JScrollPane getjscrolltable() {
		if(jscrolltable==null){
			jscrolltable = new JScrollPane(getjtable());
		}
		return jscrolltable;
	}
	public JPanel getJpadmin() {
		if(jpadmin==null){
			jpadmin=new JPanel();
			GroupLayout gl= new GroupLayout(jpadmin);
			jpadmin.setLayout(gl);
			gl.linkSize(SwingConstants.HORIZONTAL, getJlanswer(), getJlauthor(), getJlclue1(), getJlclue2(), getJlclue3(), getJltheme());
			gl.setHorizontalGroup(gl.createParallelGroup()
					.addGroup(gl.createSequentialGroup()
							.addGap(0, 0, Integer.MAX_VALUE)
							.addComponent(getJplogin(),575,575,575)
							.addGap(0, 0, Integer.MAX_VALUE)
							)
					
					.addComponent(getJpform())
					//.addComponent(getJbmenu())
					);
			gl.setVerticalGroup(gl.createSequentialGroup()
					.addGroup(gl.createParallelGroup())
					.addComponent(getJplogin())
					.addComponent(getJpform())
					);
		}
		return jpadmin;
	}
	public JLabel getJlerror() {
		if(jlerror==null){
			jlerror = new JLabel("Wrong login/password");
			jlerror.setForeground(Color.RED);
			jlerror.setVisible(false);
		}
		return jlerror;
	}
	public JScrollPane getJscrolluser() {
		if(jscrolluser==null){
			jscrolluser = new JScrollPane(getJpcolor());
		}
		return jscrolluser;
	}
	public JScrollPane getJscrolladmin(){
		if(jscrolladmin==null){
			jscrolladmin=new JScrollPane(getJpadmin());
		}
		return jscrolladmin;
	}

	private JTextField jtfClueTime ;
	public JLabel getJlClueTime() {
		if (jlClueTime == null){
			jlClueTime = new JLabel("Clue time (s)") ;
		}
		return jlClueTime;
	}
	public JTextField getJtfClueTime() {
		if (jtfClueTime == null){
			jtfClueTime = new JTextField() ;
		}
		return jtfClueTime;
		
	}
	public JPanel getJpTimeManagement()
	{
		if(jpTimeManagement==null)
		{
			jpTimeManagement= new JPanel();
			GroupLayout gl = new GroupLayout(jpTimeManagement);
			jpTimeManagement.setLayout(gl);
			gl.setHorizontalGroup(gl.createSequentialGroup()
					.addGroup(gl.createParallelGroup()
							.addComponent(getJltimer())
							.addComponent(getJlClueTime())
							)
					.addGroup(gl.createParallelGroup()
							.addComponent(getJttimer())
							.addComponent(getJtfClueTime())
							)
					);
			gl.setVerticalGroup(gl.createSequentialGroup()
					.addGroup(gl.createParallelGroup()
							
							.addComponent(getJltimer())
							.addComponent(getJttimer())
							)
					.addGroup(gl.createParallelGroup()
							.addComponent(getJlClueTime())
							.addComponent(getJtfClueTime())
							)
					);
			jpTimeManagement.setBorder(BorderFactory.createTitledBorder("Time"));
			jpTimeManagement.setVisible(false);
		}
		return jpTimeManagement;
	}
	public JPanel getJpQuestion() {
		if(jpQuestion==null)
		{
			jpQuestion=new JPanel();
			
			GroupLayout gl=new GroupLayout(jpQuestion);
			jpQuestion.setLayout(gl);
			gl.setAutoCreateContainerGaps(true);
			gl.setAutoCreateGaps(true);
			gl.linkSize(SwingConstants.HORIZONTAL, getJlanswer(), getJlauthor(), getJlclue1(), getJlclue2(), getJlclue3());
			gl.linkSize(SwingConstants.VERTICAL,getJlanswer(), getJlauthor(), getJlclue1(), getJlclue2(), getJlclue3());
			gl.setHorizontalGroup(gl.createParallelGroup()
					.addComponent(getJblogout())
					.addComponent(getJpTimeManagement())
					.addGroup(gl.createSequentialGroup()
							.addComponent(getJlauthor())
							.addComponent(getJtauthor(),300,300,Integer.MAX_VALUE)
							)
					.addGroup(gl.createSequentialGroup()
							.addComponent(getJltheme())
							.addComponent(getJttheme())
							)
					.addGroup(gl.createSequentialGroup()
							.addComponent(getJlclue1())
							.addComponent(getJtclue1())
							)
					.addGroup(gl.createSequentialGroup()
							.addComponent(getJlclue2())
							.addComponent(getJtclue2())
							)
					.addGroup(gl.createSequentialGroup()
							.addComponent(getJlclue3())
							.addComponent(getJtclue3())
							)
					.addGroup(gl.createSequentialGroup()
							.addComponent(getJlanswer())
							.addComponent(getJtanswer())
							)
					.addGroup(gl.createSequentialGroup()
							.addComponent(getJbadd())
							.addComponent(getJbApply())
							.addComponent(getJbCancel())
							)
					);
			gl.setVerticalGroup(gl.createSequentialGroup()
					.addComponent(getJblogout())
					.addComponent(getJpTimeManagement())
					.addGroup(gl.createParallelGroup()
							.addComponent(getJlauthor())
							.addComponent(getJtauthor())
							)
					.addGroup(gl.createParallelGroup()
							.addComponent(getJltheme())
							.addComponent(getJttheme())
							)
					.addGroup(gl.createParallelGroup()
							.addComponent(getJlclue1())
							.addComponent(getJtclue1())
							)
					.addGroup(gl.createParallelGroup()
							.addComponent(getJlclue2())
							.addComponent(getJtclue2())
							)
					.addGroup(gl.createParallelGroup()
							.addComponent(getJlclue3())
							.addComponent(getJtclue3())
							)
					.addGroup(gl.createParallelGroup()
							.addComponent(getJlanswer())
							.addComponent(getJtanswer())
							)
					.addGroup(gl.createParallelGroup()
							.addComponent(getJbadd())
							.addComponent(getJbApply())
							.addGap(0, 100, Integer.MAX_VALUE)
							.addComponent(getJbCancel())
							)
					);
			
			/*
			 * this loop disable every components in jpform
			 */
			components = jpQuestion.getComponents(); 
			for(Component c: components){
				//c.setFocusable(false);
				c.disable();
				if(c instanceof JButton){
					c.setVisible(false);
				}
			}
			//getjtable().setVisible(false);
			getJbSave().setVisible(false);
		}
		return jpQuestion;
	}
	public JLabel getJladmin() {
		if(jladmin==null){
			jladmin= new JLabel("Admin : ");
		}
		return jladmin;
	}
	public JLabel getJlpwd() {
		if(jlpwd==null){
			jlpwd= new JLabel("Password : ");
		}
		return jlpwd;
	}
	public JTextField getJtadmin() {
		if(jtadmin==null){
			jtadmin= new JTextField();
		}
		return jtadmin;
	}
	public JPasswordField getJppwd() {
		if(jppwd==null){
			jppwd= new JPasswordField();
			jppwd.addKeyListener(new KeyListener() {
				
				@Override
				public void keyTyped(KeyEvent arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void keyReleased(KeyEvent arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void keyPressed(KeyEvent e) {
					// TODO Auto-generated method stub
					if(e.getKeyCode()==10)
						verifPSW();
				}
			});
		}
		return jppwd;
	}

	public JButton getJblogout() {
		if(jblogout==null){
			jblogout= new JButton();
			jblogout.setText("Log out");
			jblogout.setVisible(false);
			jblogout.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					// TODO Auto-generated method stub
					jplogin.setVisible(true);
					for(Component c: components){
						c.disable();
						if(c instanceof JTextField){
							((JTextField) c).setText("");
						}
					}
					//
					getJbCancel().doClick();
					jblogout.setVisible(false);
					jbadd.setVisible(false);
					jbdel.setVisible(false);
					getJbApply().setVisible(false);
					jbModify.setVisible(false);
					getJbSave().setVisible(false);
					getjscrolltable().show(false);
					getJpTimeManagement().setVisible(false);
					
				}
			});
		}
		return jblogout;
	}
	public JButton getJbSave() {
		if(jbSave==null)
		{
			jbSave = new JButton("SAVE");
			jbSave.setPreferredSize(new Dimension(1000, 50));
			jbSave.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					Deck.getInstance().saveQuestions();
				}
			});
		}
		return jbSave;
	}
	public JButton getJblogin() {
		if(jblogin==null){
			jblogin=new JButton();
			jblogin.setText("Log in");
			jblogin.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					// TODO Auto-generated method stub
					verifPSW();
				}
			});
		}
		return jblogin;
	}
	public JButton getJbModify() {
		if(jbModify==null)
		{
			jbModify = new JButton("Modify");
			jbModify.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e){
					row = getjtable().getSelectedRow();
					if(row!=-1){
						getJbApply().setVisible(true);
						getJbCancel().setVisible(true);
						getJbadd().setVisible(false);
						Question question = Deck.getInstance().getAllQuestions().get(row);
						getJtauthor().setText(question.getAuthor());
						getJttheme().setText(question.getTheme());
						List<String> clues = question.getClues();
						getJtclue1().setText(clues.get(0));
						getJtclue2().setText(clues.get(1));
						getJtclue3().setText(clues.get(2));
						getJtanswer().setText(question.getAnswer());
					}
					//Deck.getInstance().deleteQuestion(row);
				}
			});
		}
		return jbModify;
	}
	
	public JButton getJbApply() {
		if(jbApply==null){
			jbApply = new JButton("Apply");
			jbApply.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					// TODO Auto-generated method stub
					
					Deck.getInstance().setQuestion(row, new Question(getJtauthor().getText(),getJtanswer().getText(),getJttheme().getText(),getJtclue1().getText(),
					getJtclue2().getText(),getJtclue3().getText()));
					getJbCancel().doClick();
					row=-1;
				}
			});
		}
		return jbApply;
	}
	
	public JButton getJbCancel() {
		if(jbCancel==null){
			jbCancel = new JButton("Cancel");
			jbCancel.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					getJbApply().setVisible(false);
					getJbadd().setVisible(true);
					getJbCancel().setVisible(false);
					for(Component c: components){
						if(c instanceof JTextField){
							((JTextField) c).setText("");
						}
					}
				}
			});
		}
		return jbCancel;
	}
	/*
	 * this method verify the password && login
	 */
	public void verifPSW(){
		if(jtadmin.getText().equals(login) && jppwd.getText().equals(pwd)){
			jplogin.setVisible(false);
			jblogout.setVisible(true);
			jbadd.setVisible(true);
			jbdel.setVisible(true);
			jbModify.setVisible(true);
			getjscrolltable().show(true);
			getJpTimeManagement().setVisible(true);
			getJbSave().setVisible(true);
			getJpTab().setVisible(true);
			for(Component c: components){
				getJlerror().setVisible(false);
				getJtadmin().setText("");
				getJppwd().setText("");
				c.enable();
			}
		}else{
			getJppwd().setText("");
			getJlerror().setVisible(true);
			//System.out.println("pas bon");
		}
	}
	/*
	 * add the new question in the list "questions"
	 */
	public JButton getJbadd() {
		if(jbadd==null){
			jbadd=new JButton();
			jbadd.setText("Add");
			jbadd.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					// TODO Auto-generated method stub
					Deck.getInstance().addQuestion(getJtauthor().getText(), getJtanswer().getText(), getJttheme().getText(), getJtclue1().getText(), getJtclue2().getText(), getJtclue3().getText());
				}
			});
		}
		return jbadd;
	}
	public JLabel getJlauthor() {
		if(jlauthor==null){
			jlauthor= new JLabel("Author");
		}
		return jlauthor;
	}
	public JLabel getJltheme() {
		if(jltheme==null){
			jltheme= new JLabel("Theme");
		}
		return jltheme;
	}
	public JLabel getJlanswer() {
		if(jlanswer==null){
			jlanswer= new JLabel("Answer");
		}
		return jlanswer;
	}
	public JTextField getJtauthor() {
		if(jtauthor==null){
			jtauthor= new JTextField();
		}
		return jtauthor;
	}
	public JTextField getJttheme() {
		if(jttheme==null){
			jttheme= new JTextField();
		}
		return jttheme;
	}
	public JTextField getJtanswer() {
		if(jtanswer==null){
			jtanswer= new JTextField();
		}
		return jtanswer;
	}
	
	public JLabel getJlclue1() {
		if(jlclue1==null){
			jlclue1=new JLabel("Clue 1");
		}
		return jlclue1;
	}
	public JLabel getJlclue2() {
		if(jlclue2==null){
			jlclue2=new JLabel("Clue 2");
		}
		return jlclue2;
	}
	public JLabel getJlclue3() {
		if(jlclue3==null){
			jlclue3=new JLabel("Clue 3");
		}
		return jlclue3;
	}
	public JTextField getJtclue1() {
		if(jtclue1==null){
			jtclue1= new JTextField();
		}
		return jtclue1;
	}
	public JTextField getJtclue2() {
		if(jtclue2==null){
			jtclue2= new JTextField();
		}
		return jtclue2;
	}
	public JTextField getJtclue3() {
		if(jtclue3==null){
			jtclue3= new JTextField();
		}
		return jtclue3;
	}
	public JLabel getJltimer() {
		if(jltimer==null){
			jltimer=new JLabel("Time in game (s)");
		}
		return jltimer;
	}
	public JTextField getJttimer() {		
		if(jttimer==null){
			jttimer=new JTextField();
		}
		return jttimer;
	}
	public JButton getJbmenu() {
		if(jbmenu==null){
			jbmenu = new JButton();
			jbmenu.setText("Menu");
			jbmenu.setFont(font);
			jbmenu.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					GraphicalInterface tmp =(GraphicalInterface) getJpoptionadmin().getTopLevelAncestor();
					if(getJtfClueTime().getText().length()>0)
					{
						tmp.getJpGame().clueTime= Long.valueOf(getJtfClueTime().getText())*1000;
					}
					if(getJttimer().getText().length()>0)
					{
						tmp.getJpGame().maxTime= Integer.valueOf(getJttimer().getText());
						tmp.getJpGame().time= Integer.valueOf(getJttimer().getText());
					}
					getJblogout().doClick();
					SoundPlayer.playMainSound("MenuSound.wav");
					tmp.changePanel("Menu");
				}
			});
		}
		return jbmenu;
	}
	/*
	 * we save the new list in json file with this button
	 */
	public JButton getJbapply() {
		if(jbbackground==null){
			jbbackground = new JButton();
			jbbackground.setText("Apply");
			jbbackground.setFont(font);
			jbbackground.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					
				}
			});
		}
		return jbbackground;
	}
	public JColorChooser getJcbackground() {
		if(jcbackground==null){
			jcbackground=new JColorChooser();		
			jcbackground.removeChooserPanel(jcbackground.getChooserPanels()[0]);
			jcbackground.removeChooserPanel(jcbackground.getChooserPanels()[0]);
			jcbackground.removeChooserPanel(jcbackground.getChooserPanels()[0]);
			jcbackground.removeChooserPanel(jcbackground.getChooserPanels()[1]);
			jcbackground.setPreviewPanel(new JPanel());
			jcbackground.setColor(Design.getBgpanel());
		}
		return jcbackground;
	}
	public JColorChooser getJcfontcolor() {
		if(jcfontcolor==null){
			jcfontcolor=new JColorChooser();
			jcfontcolor.removeChooserPanel(jcfontcolor.getChooserPanels()[0]);
			jcfontcolor.removeChooserPanel(jcfontcolor.getChooserPanels()[0]);
			jcfontcolor.removeChooserPanel(jcfontcolor.getChooserPanels()[0]);
			jcfontcolor.removeChooserPanel(jcfontcolor.getChooserPanels()[1]);
			jcfontcolor.setPreviewPanel(new JPanel());
			jcbackground.setColor(Design.getFontcolor());
		}
		return jcfontcolor;
	}
	public JColorChooser getJcbordercolor() {
		if(jcbordercolor==null){
			jcbordercolor=new JColorChooser();
			jcbordercolor.removeChooserPanel(jcbordercolor.getChooserPanels()[0]);
			jcbordercolor.removeChooserPanel(jcbordercolor.getChooserPanels()[0]);
			jcbordercolor.removeChooserPanel(jcbordercolor.getChooserPanels()[0]);
			jcbordercolor.removeChooserPanel(jcbordercolor.getChooserPanels()[1]);
			jcbordercolor.setPreviewPanel(new JPanel());
			jcbackground.setColor(Design.getBorderbutton());
		}
		return jcbordercolor;
	}
	public JColorChooser getJcbuttoncolor() {
		if(jcbuttoncolor==null){
			jcbuttoncolor=new JColorChooser();
			jcbuttoncolor.removeChooserPanel(jcbuttoncolor.getChooserPanels()[0]);
			jcbuttoncolor.removeChooserPanel(jcbuttoncolor.getChooserPanels()[0]);
			jcbuttoncolor.removeChooserPanel(jcbuttoncolor.getChooserPanels()[0]);
			jcbuttoncolor.removeChooserPanel(jcbuttoncolor.getChooserPanels()[1]);
			jcbuttoncolor.setPreviewPanel(new JPanel());
			jcbackground.setColor(Design.getBgbutton());
		}
		return jcbuttoncolor;
	}
	
	private JTabbedPane jtpoptiononglet;
	public JLabel getJlbackground() {
		if(jlbackground==null){
			jlbackground= new JLabel("Bakground color");
		}
		return jlbackground;
	}
	public JLabel getJljpolicecolor() {
		if(jljpolicecolor==null){
			jljpolicecolor= new JLabel("Font color");
		}
		return jljpolicecolor;
	}
	public JLabel getJlbuttoncolor() {
		if(jlbuttoncolor==null){
			jlbuttoncolor= new JLabel("Button color");
		}
		return jlbuttoncolor;
	}
	public JLabel getJlbordercolor() {
		if(jlbordercolor==null){
			jlbordercolor= new JLabel("Button border color");
		}
		return jlbordercolor;
	}
	
	/*
	 * we can switch the panel with that (jpoptionuser & jpotinadmin)
	 */
	public JTabbedPane getJtpoptiononglet() {
		if(jtpoptiononglet==null){
			jtpoptiononglet=new JTabbedPane();
			jtpoptiononglet.add("User",getJpoptionuser());
			jtpoptiononglet.add("Admin",getJpoptionadmin());
		}
		return jtpoptiononglet;
	}
	
	/*
	 * this panel contains the login panel and the form panel
	 */
	public JPanel getJpoptionadmin() {
		if(jpoptionadmin==null){
			jpoptionadmin=new JPanel();
			GroupLayout gl = new GroupLayout(jpoptionadmin);
			jpoptionadmin.setLayout(gl);
			gl.setAutoCreateContainerGaps(true);
			gl.setAutoCreateGaps(true);
			gl.setHorizontalGroup(gl.createSequentialGroup()
					.addComponent(getJscrolladmin())
					);
			gl.setVerticalGroup(gl.createParallelGroup()
					.addComponent(getJscrolladmin())
					);
		}
		return jpoptionadmin;
	}
	/*
	 * this panel contains the jscrolluser
	 */
	public JPanel getJpoptionuser() {
		if(jpoptionuser==null){
			jpoptionuser=new JPanel();
			//jpoptionuser.setFocusable(false);
			//jpoptionuser.add(new JScrollBar());
			//jpoptionuser.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
			GroupLayout gl = new GroupLayout(jpoptionuser);
			jpoptionuser.setLayout(gl);
			gl.setAutoCreateContainerGaps(true);
			gl.setAutoCreateGaps(true);
			
			gl.setHorizontalGroup(gl.createParallelGroup()
					.addComponent(getJscrolluser())
					);
			gl.setVerticalGroup(gl.createSequentialGroup()
					.addComponent(getJscrolluser())
					);		
		}
		return jpoptionuser;
	}
	/*
	 * this panel allow to the user to modify the components colors of the application
	 */
	public JPanel getJpcolor() {
		if(jpcolor==null){
			jpcolor = new JPanel();
			GroupLayout gl = new GroupLayout(jpcolor);
			jpcolor.setLayout(gl);
			gl.setAutoCreateContainerGaps(true);
			gl.setAutoCreateGaps(true);
			gl.linkSize(SwingConstants.HORIZONTAL, getJlbackground(), getJljpolicecolor(), getJlbordercolor(), getJlbuttoncolor());
			gl.setHorizontalGroup(gl.createParallelGroup()
					.addGroup(gl.createSequentialGroup()
							.addComponent(getJlbackground())
							.addComponent(getJcbackground())
							)
					.addGroup(gl.createSequentialGroup()
							.addComponent(getJlbordercolor())
							.addComponent(getJcbordercolor())
							)
					.addGroup(gl.createSequentialGroup()
							.addComponent(getJlbuttoncolor())
							.addComponent(getJcbuttoncolor())
							)
					.addGroup(gl.createSequentialGroup()
							.addComponent(getJljpolicecolor())
							.addComponent(getJcfontcolor())
							)
					
					);
			
			
			gl.setVerticalGroup(gl.createSequentialGroup()
					.addGroup(gl.createParallelGroup()
							.addComponent(getJlbackground())
							.addComponent(getJcbackground())
							)
					.addGroup(gl.createParallelGroup()
							.addComponent(getJlbordercolor())
							.addComponent(getJcbordercolor())
							)
					.addGroup(gl.createParallelGroup()
							.addComponent(getJlbuttoncolor())
							.addComponent(getJcbuttoncolor())
							)
					.addGroup(gl.createParallelGroup()
							.addComponent(getJljpolicecolor())
							.addComponent(getJcfontcolor())
							)
					);
		}
		return jpcolor;
	}
	/*
	 * this panel is used about an admin login
	 * the jpForm is enable if the login and the passwaord is correct
	 */
	public JPanel getJplogin() {
		if(jplogin==null){
			jplogin=new JPanel();
			jplogin.setBorder(BorderFactory.createLineBorder(Color.black));
			GroupLayout gl = new GroupLayout(jplogin);
			jplogin.setLayout(gl);
			gl.setAutoCreateContainerGaps(true);
			gl.setAutoCreateGaps(true);
			//gl.linkSize(SwingConstants.HORIZONTAL, getJtadmin(), getJppwd());
			//gl.linkSize(SwingConstants.HORIZONTAL, getJladmin(), getJlpwd());
			gl.setHorizontalGroup(gl.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addGap(100, 800, Integer.MAX_VALUE)
					.addGroup(gl.createSequentialGroup()
							.addComponent(getJladmin(), 50,70,100)
							.addComponent(getJtadmin(), 100,200,300)
							)
					.addGroup(gl.createSequentialGroup()
							.addComponent(getJlpwd(), 50,70,100)
							.addComponent(getJppwd(), 100,200,300)
							)
							.addComponent(getJblogin())
							.addComponent(getJlerror())
					);
			gl.setVerticalGroup(gl.createSequentialGroup()
					.addGroup(gl.createParallelGroup()
							.addComponent(getJladmin(),30,30,30)
							.addComponent(getJtadmin(),30,30,30)
							)
					.addGroup(gl.createParallelGroup()
							.addComponent(getJlpwd(),30,30,30)
							.addComponent(getJppwd(),30,30,30)
							)
					.addComponent(getJblogin())
					.addComponent(getJlerror())
					);
			//components = jplogin.getComponents();
		}
		return jplogin;
	}
	/*
	 * this form adds a new question in the list "questions"
	 */
	public JPanel getJpform() {
		if(jpform==null){
			jpform=new JPanel();
			GroupLayout gl=new GroupLayout(jpform);
			jpform.setLayout(gl);
			gl.setAutoCreateContainerGaps(true);
			gl.setAutoCreateGaps(true);
			gl.setHorizontalGroup(gl.createSequentialGroup()
					.addComponent(getJpQuestion())
					.addComponent(getJpTab())
					);
			gl.setVerticalGroup(gl.createParallelGroup()
					.addComponent(getJpQuestion())
					.addComponent(getJpTab())
					);
		}
		return jpform;
	}
	
	public JTable getjtable(){
		if(jtable==null){
			jtable=new JTable(new ModelTableau());
			jtable.getTableHeader().setReorderingAllowed(false);
		}
		return jtable;
	}
	
	/*
	 * it's the main panel, it contains optiononglet and the jbmenu(allow the return to the main menu)
	 */
	public PanelOption(){
		GroupLayout gl = new GroupLayout(this);
		this.setLayout(gl);
		gl.setAutoCreateContainerGaps(true);
		gl.setAutoCreateGaps(true);
		gl.setHorizontalGroup(gl.createParallelGroup()
				.addComponent(getJtpoptiononglet(),100,500,Integer.MAX_VALUE)
				.addComponent(getJbmenu(),40,40,Integer.MAX_VALUE)
				);
		gl.setVerticalGroup(gl.createSequentialGroup()
				.addComponent(getJtpoptiononglet(),100,500,Integer.MAX_VALUE)
				.addComponent(getJbmenu(),40,40,40)
				);
		
		
	}
	public JPanel getJpTab() {
		
		if(jpTab==null){
			jpTab=new JPanel();
			GroupLayout groupLayout = new GroupLayout(jpTab);
			jpTab.setLayout(groupLayout);
			//groupLayout.linkSize(SwingConstants.HORIZONTAL, tableau,getJbSave());
			groupLayout.setHorizontalGroup(groupLayout.createParallelGroup()
					.addComponent(getjscrolltable())
					.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createSequentialGroup()
									.addComponent(getJbdel(),0,100,Integer.MAX_VALUE)
									.addComponent(getJbModify(),0,100,Integer.MAX_VALUE)
									)
							.addComponent(getJbSave(),0,100,Integer.MAX_VALUE)
							)	
					.addComponent(getJbSave())
					);
			groupLayout.setVerticalGroup(groupLayout.createSequentialGroup()
					.addComponent(getjscrolltable())
					.addGroup(groupLayout.createParallelGroup()
							.addGroup(groupLayout.createParallelGroup()
							
								.addComponent(getJbdel())
								.addComponent(getJbModify())
							)
							.addComponent(getJbSave())
							)
					);
			Component [] compo = jpTab.getComponents();
			for(Component c: compo){
				if(c instanceof JButton){
					c.setVisible(false);
				}
			}
			getjscrolltable().show(false);
			Deck.getInstance().addObservator(this);
		}
		return jpTab;
	}
	
	public JButton getJbdel() {
		if(jbdel==null){
			jbdel=new JButton("Delete");
			jbdel.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					// TODO Auto-generated method stub
					ArrayList<Question> questions  = new ArrayList<Question>();
					Deck d= Deck.getInstance();
					int lignes[] = getjtable().getSelectedRows();
					for(int i = lignes.length-1;i>=0;i--)
					{
						Deck.getInstance().deleteQuestion(lignes[i]);
					}
					getJbCancel().doClick();
				}
			});
		}
		return jbdel;
	}
	
	private PanelOption panelOption ;
	
	public PanelOption getPanelOption() {
		if(panelOption==null)
		{
			panelOption=new PanelOption();
		}
		return panelOption;
	}
	@Override
	public void actualise() {
		((ModelTableau) getjtable().getModel()).fireTableDataChanged();
	}
}
