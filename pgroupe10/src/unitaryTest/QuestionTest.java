package unitaryTest;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import jeu.Question;

public class QuestionTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testQuestionStringStringStringListOfString() {
		List<String> clues = new ArrayList<String>();
		clues.add("foo");
		clues.add("foo");
		clues.add("foo");
		Question tmp = new Question("halonico","foo","foo",clues);
		assertNotNull(tmp);
	}

	@Test
	public void testQuestionStringStringStringStringStringString() {
		Question tmp = new Question("halonico","foo","foo","foo1","foo2","foo3");
		assertNotNull(tmp);
	}
	@Test
	public void testGetClues() {
		List<String> clues = new ArrayList<String>();
		clues.add("foo");
		clues.add("foo");
		clues.add("foo");
		Question tmp = new Question("halonico","foo","foo",clues);
		assertTrue(clues.equals(tmp.getClues()));
	}

	@Test
	public void testGetAnswer() {
		List<String> clues = new ArrayList<String>();
		clues.add("foo");
		clues.add("foo");
		clues.add("foo");
		Question tmp = new Question("halonico","foo","foo",clues);
		assertTrue("foo".equals(tmp.getAnswer()));
	}
	@Test
	public void testGetAuthor() {
		List<String> clues = new ArrayList<String>();
		clues.add("foo");
		clues.add("foo");
		clues.add("foo");
		Question tmp = new Question("halonico","foo","foo",clues);
		assertTrue("halonico".equals(tmp.getAuthor()));
	}
	@Test
	public void testToString() {
		List<String> clues = new ArrayList<String>();
		clues.add("foo");
		clues.add("foo");
		clues.add("foo");
		Question tmp = new Question("halonico","foo","foo",clues);
		assertNotNull(tmp.toString());
	}
	@Test
	public void testEquals() {
		List<String> clues = new ArrayList<String>();
		clues.add("foo");
		clues.add("foo");
		clues.add("foo");
		Question tmp = new Question("halonico","foo","foo",clues);
		Question tmp2 = new Question("halonico","foo","foo",clues);
		assertNotNull(tmp.equals(tmp2));
	}
	@Test
	public void testGetTheme() {
		List<String> clues = new ArrayList<String>();
		clues.add("foo");
		clues.add("foo");
		clues.add("foo");
		Question tmp = new Question("halonico","foo","foo",clues);
		assertTrue("foo".equals(tmp.getTheme()));
	}

	@Test
	public void testCopy() {
		List<String> clues = new ArrayList<String>();
		clues.add("foo");
		clues.add("foo");
		clues.add("foo");
		Question original = new Question("halonico","foo","foo",clues);
		Question duplicata = original.copy();
		assertTrue(duplicata.equals(original));
	}

}
