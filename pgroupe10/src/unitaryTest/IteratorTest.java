package unitaryTest;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import jeu.Deck;
import jeu.IteratorQuestion;
import jeu.Question;
import test.Explorateur;

public class IteratorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testIteratorQuestion() {
		List<Question> listeQuestions = new ArrayList<Question>();
		IteratorQuestion iterator = new IteratorQuestion(listeQuestions);
		assertNotNull(iterator);
	}

	@Test
	public void testNext() {
		List<Question> listeQuestions = new ArrayList<Question>();
		listeQuestions.add(new Question("halonico", "42", "The Truth", "what is meaning of world", "What is meaning of universe", "What is meaning of life"));
		listeQuestions.add(new Question("halonico", "42", "Null Error Exception", "what is meaning of world", "What is meaning of universe", "What is meaning of life"));
		IteratorQuestion iterator = new IteratorQuestion(listeQuestions);
		iterator.next();
		int truc = (int) Explorateur.getField(iterator, "index");
		assertTrue(truc==1);
	}

	@Test
	public void testPrevious() {
		
		List<Question> listeQuestions = new ArrayList<Question>();
		listeQuestions.add(new Question("halonico", "42", "The Truth", "what is meaning of world", "What is meaning of universe", "What is meaning of life"));
		listeQuestions.add(new Question("halonico", "42", "Null Error Exception", "what is meaning of world", "What is meaning of universe", "What is meaning of life"));
		IteratorQuestion iterator = new IteratorQuestion(listeQuestions);
		iterator.previous();
		int truc = (int) Explorateur.getField(iterator, "index");
		assertTrue(truc==-1);
	}

	@Test
	public void testSetIndex() {
		List<Question> listeQuestions = new ArrayList<Question>();
		listeQuestions.add(new Question("halonico", "42", "The Truth", "what is meaning of world", "What is meaning of universe", "What is meaning of life"));
		listeQuestions.add(new Question("halonico", "42", "Null Error Exception", "what is meaning of world", "What is meaning of universe", "What is meaning of life"));
		IteratorQuestion iterator = new IteratorQuestion(listeQuestions);
		iterator.setIndex(42);
		int truc = (int) Explorateur.getField(iterator, "index");
		assertTrue((truc==42));
	}
	//foo
	@Test
	public void testReachItem() {
		List<Question> listeQuestions = new ArrayList<Question>();
		listeQuestions.add(new Question("halonico", "42", "The Truth", "what is meaning of world", "What is meaning of universe", "What is meaning of life"));
		listeQuestions.add(new Question("halonico", "42", "Null Error Exception", "what is meaning of world", "What is meaning of universe", "What is meaning of life"));
		IteratorQuestion iterator = new IteratorQuestion(listeQuestions);
		iterator.reachItem("Null Error Exception");
		int truc = (int) Explorateur.getField(iterator, "index");
		ArrayList<Question> questions = (ArrayList<Question>) Explorateur.getField(iterator, "listeQuestions");
		assertTrue(questions.get(truc).getTheme().equals("Null Error Exception"));
	}

	@Test
	public void testItem() {
		List<Question> listeQuestions = new ArrayList<Question>();
		listeQuestions.add(new Question("halonico", "42", "The Truth", "what is meaning of world", "What is meaning of universe", "What is meaning of life"));
		listeQuestions.add(new Question("halonico", "42", "Null Error Exception", "what is meaning of world", "What is meaning of universe", "What is meaning of life"));
		IteratorQuestion iterator = new IteratorQuestion(listeQuestions);
		assertNotNull(iterator.item());
	}

}
