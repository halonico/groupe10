package unitaryTest;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import jeu.Deck;
import jeu.IteratorQuestion;
import jeu.Question;
import test.Explorateur;

public class DeckTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		
	}

	@Test
	public void testLoadQuestions() {
		Deck.getInstance().loadQuestions();
		ArrayList<Question> questions = (ArrayList<Question>) Explorateur.getField(Deck.getInstance(), "questions");
		assertNotNull(questions);
	}

	@Test
	public void testGetInstance() {
		assertNotNull(Deck.getInstance());
	}

	@Test
	public void testAddQuestionStringStringStringStringStringString() {
		Deck.getInstance().addQuestion("Halonico", "Pizza", "Food", "I'm a popular Italian meal", "I'm mostly made of bread dough", "I'm circular");
		ArrayList<Question> questions = (ArrayList<Question>) Explorateur.getField(Deck.getInstance(), "questions");
		assertTrue(questions.contains(new Question("Halonico", "Pizza", "Food", "I'm a popular Italian meal", "I'm mostly made of bread dough", "I'm circular")));
	}

	@Test
	public void testAddQuestionQuestion() {
		Deck.getInstance().addQuestion(new Question("Halonico", "blblblbl", "blblblbl", "I'm a popular Italian meal", "I'm mostly made of bread dough", "I'm circular"));
		ArrayList<Question> questions = (ArrayList<Question>) Explorateur.getField(Deck.getInstance(), "questions");
		assertTrue(questions.contains(new Question("Halonico", "blblblbl", "blblblbl", "I'm a popular Italian meal", "I'm mostly made of bread dough", "I'm circular")));
	}

	@Test
	public void testDeleteQuestion() {
		ArrayList<Question> questions = (ArrayList<Question>) Explorateur.getField(Deck.getInstance(), "questions");
		Question tmp = questions.get(0).copy();
		Deck.getInstance().deleteQuestion(0);
		assertFalse(questions.contains(tmp));
	}

	@Test
	public void testSetQuestion() {
		Question tmp = new Question("Halonico", "Pizza", "Food", "I'm a popular Italian meal", "I'm mostly made of bread dough", "I'm circular");
		Deck.getInstance().setQuestion(0, tmp);
		ArrayList<Question> questions = (ArrayList<Question>) Explorateur.getField(Deck.getInstance(), "questions");
		assertTrue(questions.get(0).equals(tmp));
	}

	@Test
	public void testGetAllQuestions() {
		ArrayList<Question> questions =Deck.getInstance().getAllQuestions();
		assertNotNull(questions);
	}

	@Test
	public void testGetIterator() {
		IteratorQuestion iterator;
		iterator = Deck.getInstance().getIterator();
		assertNotNull(iterator);
	}

	@Test
	public void testGetThemes() {
		ArrayList<String> themes = Deck.getInstance().getThemes();
		assertNotNull(themes);
	}

	@Test
	public void testSaveQuestions() {
		Deck.getInstance().saveQuestions();
		assertTrue(true);
	}

	@Test
	public void testGet() {
		ArrayList<Question> questions = (ArrayList<Question>) Explorateur.getField(Deck.getInstance(), "questions");
		Question tmp = Deck.getInstance().get(0);
		assertTrue(questions.get(0).equals(tmp));
	}

}
