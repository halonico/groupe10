package unitaryTest;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import jeu.GestionScore;
import jeu.Score;
import test.Explorateur;

public class testGestionScore {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testLoadScores() {
		GestionScore.loadScores();
		ArrayList<Score> scores = (ArrayList<Score>) Explorateur.getField(new GestionScore(), "scores");
		assertNotNull(scores);
	}

	@Test
	public void testSaveScores() {
		GestionScore.saveScores();
		assertTrue(true);
	}

	@Test
	public void testAddScore() {
		Score tmp = new Score("halonico", 42, 42, 42);
		GestionScore.addScore(tmp);
		ArrayList<Score> scores = (ArrayList<Score>) Explorateur.getField(new GestionScore(), "scores");
		assertTrue(scores.contains(tmp));
	}

	@Test
	public void testGetScores() {
		
		ArrayList<Score> scores1 = (ArrayList<Score>) Explorateur.getField(new GestionScore(), "scores");
		ArrayList<Score> scores2 = GestionScore.getScores();
		assertTrue(scores1.equals(scores2));
	}

}
